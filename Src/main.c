/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "u8g2.h"
#include "math.h"
#include "string.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc;
DMA_HandleTypeDef hdma_adc;

I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim3;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
uint32_t uwDutyCycle ;
double currentAngle;

//#define PRINT_DEBUG

uint16_t adcData[3];

#define PI 			3.14159265
#define rad2deg 	180.0/PI

//u8g2 stuff
u8g2_t u8g2_iic;


#define UNUSEDVAR __attribute__ ((unused))

typedef enum {
  HI_WORLD,
  LYDIA_PIC,
  STOCK_U8G2,
  TIMECODE,
  LINES,
  LAST_IMG
} IMGS;

void byte_at_string(uint8_t *str, int i)
{

  str[2] = '0' + i%10;
  str[1] = '0' + (i/10)%10;
  str[0] = '0' + (i/100)%10;

}
void btoa(uint8_t i, uint8_t *str)
{
  str[2] = '0' + i%10;
  str[1] = '0' + (i/10)%10;
  str[0] = '0' + (i/100)%10;
}

void itoa(uint32_t i, uint8_t *str)
{
  str[4] = '0' + i%10;
  str[3] = '0' + (i/10)%10;
  str[2] = '0' + (i/100)%10;
  str[1] = '0' + (i/1000)%10;
  str[0] = '0' + (i/10000)%10;
}

void hex_at_string(uint8_t *str, int i)
{
  uint8_t lower = i&0xF;
  uint8_t upper = (i&0xF0) >> 4;

  if (lower < 10) {
    str[1] = '0' + lower;
  } else {
    str[1] = 'A' + lower - 10;
  }
  if (upper < 10) {
    str[0] = '0' + upper;
  } else {
    str[0] = 'A' + upper - 10;
  }
}


int my_I2C_GET_FLAG(I2C_HandleTypeDef* __HANDLE__,
                    uint32_t __FLAG__)
{
	HAL_Delay(100);
  // #define I2C_FLAG_BUSY                   ((uint32_t)0x0010 0002)
  // #define I2C_FLAG_MASK                   ((uint32_t)0x0000 FFFF)
	/*
  if (((uint8_t)((__FLAG__) >> 16)) == 0x01) {
    return (__HANDLE__->Instance->SR1 & __FLAG__ & I2C_FLAG_MASK) == (__FLAG__ & I2C_FLAG_MASK);
  } else {
    return (__HANDLE__->Instance->SR2 & __FLAG__ & I2C_FLAG_MASK) == (__FLAG__ & I2C_FLAG_MASK);
  }
  */
	return 1;
}

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC_Init(void);
static void MX_I2C1_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM3_Init(void);
static void MX_USART1_UART_Init(void);
/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
float iDUTY = 0	;
int iSetFreq = 0;

volatile uint32_t counter=0;

void User_GetAngle(void);

volatile uint8_t haveValue =0;
volatile uint8_t haveAngle =0;

uint32_t amountsent =0;
uint8_t u8x8_byte_my_hw_i2c(
  U8X8_UNUSED u8x8_t *u8x8,
  U8X8_UNUSED uint8_t msg,
  U8X8_UNUSED uint8_t arg_int,
  U8X8_UNUSED void *arg_ptr);

uint8_t u8x8_gpio_and_delay_mine(
u8x8_t *u8x8,
uint8_t msg,
uint8_t arg_int,
void *arg_ptr);

void Refresh_OLED(u8g2_t *u8g2, IMGS img);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC_Init();
  MX_I2C1_Init();
  MX_TIM1_Init();
  MX_TIM3_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */

  /*##-5- Start the Input Capture in interrupt mode ##########################*/
  if (HAL_TIM_IC_Start(&htim3, TIM_CHANNEL_1) != HAL_OK)
  {
    /* Starting Error */
    Error_Handler();
  }
  if (HAL_TIM_IC_Start(&htim3, TIM_CHANNEL_2) != HAL_OK)
  {
    /* Starting Error */
    Error_Handler();
  }
  if (HAL_TIM_IC_Start(&htim3, TIM_CHANNEL_3) != HAL_OK)
  {
    /* Starting Error */
    Error_Handler();
  }
  if (HAL_TIM_IC_Start_IT(&htim3, TIM_CHANNEL_4) != HAL_OK)
  {
    /* Starting Error */
    Error_Handler();
  }

  if (HAL_ADCEx_Calibration_Start(&hadc) !=  HAL_OK)
  {
      /* ADC Calibration Error */
      Error_Handler();
  }

  //start the ADC
  HAL_ADC_Start_DMA(&hadc, (uint32_t*)&adcData, 3);

  //start the ADC timer
  HAL_TIM_Base_Start(&htim1);


  //1 at the end sets to one page mode
  u8g2_Setup_ssd1306_i2c_128x64_noname_1(&u8g2_iic,
                                         U8G2_R0,
                                         u8x8_byte_my_hw_i2c,
                                         u8x8_gpio_and_delay_mine);
  u8g2_InitDisplay(&u8g2_iic); // send init sequence to the display, display is in sleep mode after this,
  u8g2_SetPowerSave(&u8g2_iic, 0); // wake up display


  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

	  if(haveValue){
		  Refresh_OLED(&u8g2_iic, TIMECODE);
		  haveValue = 0;

	  }

	  if(haveAngle){
		  User_GetAngle();
		  Refresh_OLED(&u8g2_iic, TIMECODE);
		  haveAngle =0;
	  }

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSI14;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSI14State = RCC_HSI14_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.HSI14CalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC_Init(void)
{

  /* USER CODE BEGIN ADC_Init 0 */

  /* USER CODE END ADC_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC_Init 1 */

  /* USER CODE END ADC_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
  */
  hadc.Instance = ADC1;
  hadc.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc.Init.Resolution = ADC_RESOLUTION_12B;
  hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
  hadc.Init.EOCSelection = ADC_EOC_SEQ_CONV;
  hadc.Init.LowPowerAutoWait = DISABLE;
  hadc.Init.LowPowerAutoPowerOff = DISABLE;
  hadc.Init.ContinuousConvMode = DISABLE;
  hadc.Init.DiscontinuousConvMode = DISABLE;
  hadc.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T1_TRGO;
  hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_RISING;
  hadc.Init.DMAContinuousRequests = ENABLE;
  hadc.Init.Overrun = ADC_OVR_DATA_OVERWRITTEN;
  if (HAL_ADC_Init(&hadc) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel to be converted. 
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
  sConfig.SamplingTime = ADC_SAMPLETIME_41CYCLES_5;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel to be converted. 
  */
  sConfig.Channel = ADC_CHANNEL_1;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel to be converted. 
  */
  sConfig.Channel = ADC_CHANNEL_VBAT;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC_Init 2 */

  /* USER CODE END ADC_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x2000090E;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter 
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter 
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 120;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 62499;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_IC_InitTypeDef sConfigIC = {0};
  TIM_SlaveConfigTypeDef sSlaveConfig = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 80;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 0xFFFF;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_IC_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_FALLING;
  sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
  sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
  sConfigIC.ICFilter = 0;
  if (HAL_TIM_IC_ConfigChannel(&htim3, &sConfigIC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
  sConfigIC.ICSelection = TIM_ICSELECTION_INDIRECTTI;
  if (HAL_TIM_IC_ConfigChannel(&htim3, &sConfigIC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  sSlaveConfig.SlaveMode = TIM_SLAVEMODE_RESET;
  sSlaveConfig.InputTrigger = TIM_TS_TI1FP1;
  sSlaveConfig.TriggerPolarity = TIM_INPUTCHANNELPOLARITY_FALLING;
  sSlaveConfig.TriggerPrescaler = TIM_ICPSC_DIV1;
  sSlaveConfig.TriggerFilter = 0;
  if (HAL_TIM_SlaveConfigSynchronization(&htim3, &sSlaveConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_FALLING;
  sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
  if (HAL_TIM_IC_ConfigChannel(&htim3, &sConfigIC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
  sConfigIC.ICSelection = TIM_ICSELECTION_INDIRECTTI;
  if (HAL_TIM_IC_ConfigChannel(&htim3, &sConfigIC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

}

/* USER CODE BEGIN 4 */
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim){
	static char data[50];
	static uint32_t lastTrigger = 0;
	uint16_t readValue[4]={0,0,0,0};
	static uint16_t prevReadValue[4] = {0,0,0,0};

	//timer_tick_frequency = Timer_default_frequency / (prescaller_set + 1)
//   = 8000000/(80+1) // htim3.Init.Prescaler = 80
	// time = 1/f
	if(htim->Instance == TIM3){// time from ch1 to ch4

		//(1000/50)*time = m/sec ... 50 is dispance between leds
			readValue[0] = (HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1));
			readValue[1] = (HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_2));
			readValue[2] = (HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_3));
			readValue[3] = (HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_4));


			if(HAL_GetTick() - lastTrigger < 100){
				//this is a dud
				uwDutyCycle = prevReadValue[3]; //29;
			} else if(prevReadValue[0]==readValue[0]){
				//this is a dud
				uwDutyCycle = 19;
			} else if (readValue[3]-readValue[2]>500){
				uwDutyCycle = readValue[2];
				haveValue=1;
			}


			memcpy(prevReadValue, readValue, sizeof(readValue));

			lastTrigger = HAL_GetTick();

			uint8_t len = sprintf(&data[0],"%i\t%i\t%i\t%i\t%li\t%i\n",
					readValue[0],readValue[1],readValue[2],	readValue[3],uwDutyCycle, readValue[3]-readValue[1]);

			HAL_UART_Transmit(&huart1,(uint8_t*)&data,len,1000);
		}

}

void HAL_TIM_PWM_PulseFinishedCallback(TIM_HandleTypeDef *htim){

	counter++;
}

uint8_t
u8x8_byte_my_hw_i2c(
  U8X8_UNUSED u8x8_t *u8x8,
  U8X8_UNUSED uint8_t msg,
  U8X8_UNUSED uint8_t arg_int,
  U8X8_UNUSED void *arg_ptr)
{
#define MAX_LEN 32
  static uint8_t vals[MAX_LEN];
  static uint8_t length=0;

  U8X8_UNUSED uint8_t *args = arg_ptr;
  switch(msg)  {
  case U8X8_MSG_BYTE_SEND: {
    if ((arg_int+length) <= MAX_LEN) {
      for(int i=0; i<arg_int; i++) {
        vals[length] = args[i];
        length++;
      }
    } else {
      uint8_t umsg[] = "MSG_BYTE_SEND arg too long xxx\n";
      byte_at_string(umsg+27, arg_int);
      //sendUARTmsgPoll(umsg, sizeof(umsg));
    }
    uint8_t umsg[] = "MSG_BYTE_SEND 0xxx\n";
    // 00 AE = display off
    // 00 D5 = set display clock
    // 00 80    divide ratio
    // 00 A8 = set multiplex ratio
    // 00 3F    64 mux
    // 00 D3 = set display offset
    // 00 00
    // 00 40 - 00 7F set display start line


    hex_at_string(umsg+16, args[0]);
    //sendUARTmsgPoll(umsg, sizeof(umsg));
    break;
  }
  case U8X8_MSG_BYTE_INIT: {
    uint8_t umsg[] = "MSG_BYTE_INIT xxx\n";
    byte_at_string(umsg+14, arg_int);
    //sendUARTmsgPoll(umsg, sizeof(umsg));
    break;
  }
  case U8X8_MSG_BYTE_SET_DC: {
    uint8_t umsg[] = "MSG_BYTE_SET_DC xxx\n";
    byte_at_string(umsg+15, arg_int);
    //sendUARTmsgPoll(umsg, sizeof(umsg));
    break;
  }
  case U8X8_MSG_BYTE_START_TRANSFER: {
    UNUSEDVAR uint8_t umsg[] = "MSG_BYTE_START\n";
    //sendUARTmsgPoll(umsg, sizeof(umsg));
    length = 0;
    break;
  }
  case U8X8_MSG_BYTE_END_TRANSFER: {
    UNUSEDVAR uint8_t umsg[] = "MSG_BYTE_END xxxxx\n";
    itoa(length, umsg+13);
    //sendUARTmsgPoll(umsg, sizeof(umsg));
    while(HAL_I2C_GetState (&hi2c1) != HAL_I2C_STATE_READY) { /* empty */ }
    const uint8_t addr = 0x78;
    HAL_I2C_Master_Transmit(&hi2c1, addr, vals, length, 10);
    amountsent+= length;
    break;
  }
  default: {
    uint8_t umsg[] = "MSG_BYTE_DEFAULT xxx\n";
    byte_at_string(umsg+17, arg_int);
    //sendUARTmsgPoll(umsg, sizeof(umsg));
    return 0;
  }
  }
  return 1;
}

uint8_t
u8x8_gpio_and_delay_mine(
u8x8_t *u8x8,
uint8_t msg,
uint8_t arg_int,
void *arg_ptr)
{

  switch(msg)
  {
    case U8X8_MSG_GPIO_AND_DELAY_INIT:
      /* only support for software I2C*/
      break;

    case U8X8_MSG_DELAY_NANO:
      /* not required for SW I2C */
      break;

    case U8X8_MSG_DELAY_10MICRO:
      /* not used at the moment */
      break;

    case U8X8_MSG_DELAY_100NANO:
      /* not used at the moment */
      break;

    case U8X8_MSG_DELAY_MILLI:
      //delay_micro_seconds(arg_int*1000UL);
      break;

    case U8X8_MSG_DELAY_I2C:
      /* arg_int is 1 or 4: 100KHz (5us) or 400KHz (1.25us) */
      //delay_micro_seconds(arg_int<=2?5:1);
      break;

    case U8X8_MSG_GPIO_I2C_CLOCK:
      break;

    case U8X8_MSG_GPIO_I2C_DATA:
      break;
/*
    case U8X8_MSG_GPIO_MENU_SELECT:
      u8x8_SetGPIOResult(u8x8, Chip_GPIO_GetPinState(LPC_GPIO, KEY_SELECT_PORT, KEY_SELECT_PIN));
      break;
    case U8X8_MSG_GPIO_MENU_NEXT:
      u8x8_SetGPIOResult(u8x8, Chip_GPIO_GetPinState(LPC_GPIO, KEY_NEXT_PORT, KEY_NEXT_PIN));
      break;
    case U8X8_MSG_GPIO_MENU_PREV:
      u8x8_SetGPIOResult(u8x8, Chip_GPIO_GetPinState(LPC_GPIO, KEY_PREV_PORT, KEY_PREV_PIN));
      break;

    case U8X8_MSG_GPIO_MENU_HOME:
      u8x8_SetGPIOResult(u8x8, Chip_GPIO_GetPinState(LPC_GPIO, KEY_HOME_PORT, KEY_HOME_PIN));
      break;
*/
    default:
      //u8x8_SetGPIOResult(u8x8, 1);
      break;
  }
  return 1;
}

#define N_DECIMAL_POINTS_PRECISION (100) // n = 3. Three decimal points.
#define TICK_TIME 8000/81 //msec

void Refresh_OLED(u8g2_t *u8g2, IMGS img)
{

	  enum {BufSize=9};
	  char buf[BufSize];

	  double PSC = htim3.Init.Prescaler;			//timer pre-scaler
	  double CLK = HAL_RCC_GetSysClockFreq();		//system clock
	  uint8_t distanceBetweenLeds = 50; 			//mm

/* uwCycleTime is number of clock between the two sensors
 * The clock us 8Mhz (system) / 80 (prescale) = 100,000 pulse/sec = 100pulse /ms or 1/100,000 sec/pulse
 * travel 50mm between samples or  0.05m
 * pulseCount / 100,000 = timeBetween Points
 * speed = distance/tim between points (mm/ms = m/s) *3/6 = km/hr
 *
 *
 */

	  int integerPart;
	  int decimalPart ;

	  double timeMS = (uwDutyCycle*(1.0/(TICK_TIME)));
	  integerPart = (int)timeMS;
	  decimalPart = ((int)(timeMS*N_DECIMAL_POINTS_PRECISION)%N_DECIMAL_POINTS_PRECISION);
//	  snprintf (buf, BufSize, "%i.%i", integerPart, decimalPart);

	  double speed = distanceBetweenLeds/(uwDutyCycle/((CLK/PSC)/1000))*3.6;
	  if(speed>50 || speed <0 || uwDutyCycle == 0){
		  speed  = 0;
	  }
	  integerPart = (int)speed;
	  decimalPart = ((int)(speed*N_DECIMAL_POINTS_PRECISION)%N_DECIMAL_POINTS_PRECISION);
	  snprintf (buf, BufSize, "%i.%i", integerPart, decimalPart);


	  if(uwDutyCycle == 19 ){
	  	snprintf(buf,BufSize, "ERR1");

	  }else if(uwDutyCycle == 29 ){
	  	snprintf(buf,BufSize, "ERR2");
	  }else if(uwDutyCycle == 39 ){
	  	snprintf(buf,BufSize, "ERR3");
	  }

	  static char count[] = {'0','\n'};
	  if(count[0] == '8'){
		  count[0]='0';
	  }else {
		  count[0]='8';
	  }

	  char angle[BufSize];
	  snprintf (angle, BufSize, "%i", (int16_t)currentAngle);

	  u8g2_FirstPage(u8g2);
	  do {
	    u8g2_SetFont(u8g2, u8g2_font_ncenB14_tn);
	    u8g2_DrawStr(u8g2, 0,16,angle);
	    //u8g2_DrawStr(u8g2, 118,16,count);

	    u8g2_SetFont(u8g2, u8g2_font_fub30_tn);
		u8g2_DrawStr(u8g2, 0,60, buf); //data

		u8g2_SetFont(u8g2, u8g2_font_ncenB14_tn);
	    u8g2_DrawStr(u8g2, 118,60,count);

	  } while ( u8g2_NextPage(u8g2) );

}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc){
	haveAngle = 1;

}

uint16_t XL_X;
uint16_t XL_Z;
uint8_t xlBeta = 1; // Beta Range 1 to 6
double accelAngle;

//in bits not volts
#define ADC_ref 4.096
#define zero_x 1.964 //actually y
#define zero_y 2.09
#define zero_z 2.09
#define sensitivity_x 0.4
#define sensitivity_y 0.4
#define sensitivity_z 0.4

void User_GetAngle(void){
	/*Filter the data*/
	//SmoothData(t0) = SmoothData(t-1) - (LPF_Beta * (SmoothData(t-1) - RawData));
	//XL_X 		= (adcData[0] + (XL_X << xlBeta) - XL_X) >>xlBeta;
	//XL_Z		= (adcData[1] + (XL_Z << xlBeta) - XL_Z) >>xlBeta;

	//get the g's
	double xv = (adcData[0]/4096.0*ADC_ref-zero_x)/sensitivity_x;
	double zv = (adcData[1]/4096.0*ADC_ref-zero_z)/sensitivity_z;

	//calculate the angle
	accelAngle =  rad2deg * atan((double)xv  / (double)zv );

	//check not a divide by 0
	if(accelAngle != accelAngle)accelAngle=0;

	currentAngle = accelAngle;

#ifdef PRINT_DEBUG
	static char data[20];
	uint8_t len = sprintf(&data[0],"%i\n", (int16_t)currentAngle);
	HAL_UART_Transmit(&huart1,(uint8_t*)&data,len,1000);
#endif
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
